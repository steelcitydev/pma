﻿require('rootpath')();
var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
var expressJwt = require('express-jwt');
var mongoose = require('mongoose');
var schema = mongoose.Schema;
var config = require('config.json');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var schemaName = new schema({
    request: String,
    time: Number
}, {
    collection: 'collectionName'
});

// use JWT auth to secure the api, the token can be passed in the authorization header or querystring
app.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}).unless({ path: ['/users/authenticate', '/users/register'] }));


var Model = mongoose.model('Model', schemaName);
mongoose.connect('mongodb://localhost:27017/pma');

app.get('/save:query', cors(), function(req, res) {
    var query = req.params.query;
    
    var savedata = new model({
        'request': query,
        'time': Math.floor(Date.now() / 1000) //time of the saved data.
    }).save(function(err, result) {
        if(err) throw err;
        
        if(result) {
            res.json(results)
        }
    })
})

app.get('/find/:query', cors(), function(req, res) {
    var query = req.params.query;
    
    Model.find({
        'request': query
    }, function(err, result) {
        if (err) throw err;
        if (result) {
            res.json(result)
        } else {
            res.send(JSON.stringify({
                error : 'Error'
            }))
        }
    })
})



// routes
app.use('/users', require('./controllers/users.controller'));

// start server
var port = process.env.NODE_ENV === 'production' ? 80 : 4000;
var server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});