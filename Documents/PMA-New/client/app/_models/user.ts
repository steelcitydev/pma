﻿export class User {
    _id: string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    userRole: [{
        admin: Number,
        propOwner: Number,
        propManager: Number,
        tenant: Number,
        maintenance: Number,
        officeStaff: Number
    }]
}